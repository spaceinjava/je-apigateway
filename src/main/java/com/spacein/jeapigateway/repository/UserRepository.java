package com.spacein.jeapigateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.google.common.base.Optional;
import com.spacein.base.entity.LoginUser;

public interface UserRepository extends JpaRepository<LoginUser, String>{

	Optional<LoginUser> findByUserName(String username);
	
}
