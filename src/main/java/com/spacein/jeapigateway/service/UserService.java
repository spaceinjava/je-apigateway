package com.spacein.jeapigateway.service;

import com.spacein.base.entity.LoginUser;

public interface UserService {

	LoginUser findOne(String userName);

}
