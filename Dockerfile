FROM java:8
WORKDIR /
ADD target/je-apigateway-*.jar jeapigateway.jar
EXPOSE 8082
CMD ["java", "-jar","-Dspring.profiles.active=dev", "jeapigateway.jar"]